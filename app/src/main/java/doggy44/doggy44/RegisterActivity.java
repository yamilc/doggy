package doggy44.doggy44;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    Button bRegister;
    EditText nombre, apellido, email, contrasenia, username;
    String rolseleccionado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        nombre = (EditText) findViewById(R.id.nombre);
        apellido = (EditText) findViewById(R.id.apellido);
        email = (EditText) findViewById(R.id.email);
        username = (EditText) findViewById(R.id.username);
        contrasenia = (EditText) findViewById(R.id.password);

        bRegister =  (Button) findViewById(R.id.btRegister);

        Switch simpleSwitch = (Switch) findViewById(R.id.simpleSwitch);

        if (simpleSwitch.isChecked())
            rolseleccionado = "2";
        else
            rolseleccionado = "1";




        bRegister.setOnClickListener(this);

    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.btRegister:

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);


                try {

                    Switch simpleSwitch = (Switch) findViewById(R.id.simpleSwitch);

                    if (simpleSwitch.isChecked())
                        rolseleccionado = "2";
                    else
                        rolseleccionado = "1";

                    String login_url = MyApplicationG.url + "user/add";

                    URL url = new URL(login_url);
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));


                    String post_data = URLEncoder.encode("first_name","UTF-8")+"="+URLEncoder.encode(nombre.getText().toString(),"UTF-8")+"&"
                            +URLEncoder.encode("last_name","UTF-8")+"="+URLEncoder.encode(apellido.getText().toString(),"UTF-8")+"&"
                            +URLEncoder.encode("email","UTF-8")+"="+URLEncoder.encode(email.getText().toString(),"UTF-8")+"&"
                            +URLEncoder.encode("username","UTF-8")+"="+URLEncoder.encode(username.getText().toString(),"UTF-8")+"&"
                            +URLEncoder.encode("rol","UTF-8")+"="+URLEncoder.encode(rolseleccionado,"UTF-8")+"&"
                            +URLEncoder.encode("password","UTF-8")+"="+URLEncoder.encode(contrasenia.getText().toString(),"UTF-8")+"&"

                            +URLEncoder.encode("active","UTF-8")+"="+URLEncoder.encode("1","UTF-8")+"&"
                            +URLEncoder.encode("phone","UTF-8")+"="+URLEncoder.encode("99999","UTF-8")+"&"
                            +URLEncoder.encode("address","UTF-8")+"="+URLEncoder.encode("Ercilla 111","UTF-8")+"&"
                            +URLEncoder.encode("city","UTF-8")+"="+URLEncoder.encode("Buenos Aires","UTF-8")+"&"
                            +URLEncoder.encode("country","UTF-8")+"="+URLEncoder.encode("Argentina","UTF-8");


                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                    String result="";
                    String line="";
                    while((line = bufferedReader.readLine())!= null) {
                        result += line;
                    }

                    // acá en result recibo el token

                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    Toast toast = Toast.makeText(this, "Tu usuario " + username.getText().toString() + " ha sido creado", Toast.LENGTH_SHORT);
                    toast.show();


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }



                break;
        }
    }

    public void sendMessage(View view)
    {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
    }
}
