package doggy44.doggy44;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class PaseosFragment extends Fragment {

    private String TAG = MainActivity.class.getSimpleName();

    private ProgressDialog pDialog;
    private int posicionn;
    private ListView lv;

    // URL to get contacts JSON
    private static String url = MyApplicationG.url + "walksActives";
    //private static String url = "https://api.androidhive.info/contacts/";

    ArrayList<HashMap<String, String>> contactList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_paseos, container, false);

        contactList = new ArrayList<>();
        lv = (ListView) view.findViewById(R.id.list3);
        new GetContacts3().execute();

        List<DataClass> selectedList;

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                posicionn = position;

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Confirmación");
                builder.setMessage("Desea confirmar el paseo?");
                builder.setCancelable(false);
                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), "Se ha confirmado el paseo.", Toast.LENGTH_SHORT).show();


                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                        StrictMode.setThreadPolicy(policy);

                    try {

                        String var = lv.getItemAtPosition(posicionn).toString();

                        String string = var;
                        String[] parts = string.split("walk_id");
                        String part1 = parts[0];
                        String part2 = parts[1];

                        String[] parts2 = part2.split("=");
                        String part11 = parts2[0];
                        String part22 = parts2[1];

                        int cantidad= 1;
                        String walk_id_final = part22.substring(0, part22.length()-cantidad);


                        String login_url = MyApplicationG.url + "reservarPaseo";

                        URL url = new URL(login_url);
                        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                        httpURLConnection.setRequestMethod("POST");
                        httpURLConnection.setDoOutput(true);
                        httpURLConnection.setDoInput(true);
                        OutputStream outputStream = httpURLConnection.getOutputStream();
                        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                        String post_data = URLEncoder.encode("walk_id","UTF-8")+"="+URLEncoder.encode(walk_id_final,"UTF-8")+"&"
                                +URLEncoder.encode("user_id","UTF-8")+"="+URLEncoder.encode(MyApplicationG.user_id,"UTF-8");

                        bufferedWriter.write(post_data);

                        bufferedWriter.flush();
                        bufferedWriter.close();
                        outputStream.close();
                        InputStream inputStream = httpURLConnection.getInputStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
                        String result = "";
                        String line = "";
                        while ((line = bufferedReader.readLine()) != null) {
                            result += line;
                        }

                        // acá en result recibo el token

                        bufferedReader.close();
                        inputStream.close();
                        httpURLConnection.disconnect();

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }









                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {




                    }
                });

                builder.show();







                /*Toast toast = Toast.makeText(getActivity(), walk_id_final, Toast.LENGTH_SHORT);
                toast.show();*/

            }
        });


        return view;
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetContacts3 extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Por favor espere...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node
                    JSONArray contacts = jsonObj.getJSONArray("contacts");

                    // looping through All Contacts
                    for (int i = 0; i < contacts.length(); i++) {
                        JSONObject c = contacts.getJSONObject(i);

                        String walk_id = c.getString("walk_id");
                        String user_id = c.getString("user_id");
                        String walk_date = c.getString("walk_date");
                        String return_date = c.getString("return_date");
                        String walk_time = c.getString("walk_time");
                        String first_name = c.getString("first_name");
                        String last_name = c.getString("last_name");

                        // Phone node is JSON Object
                        //JSONObject phone = c.getJSONObject("phone");
                        //String mobile = phone.getString("mobile");
                        //String home = phone.getString("home");
                        //String office = phone.getString("office");

                        // tmp hash map for single contact
                        HashMap<String, String> contact = new HashMap<>();

                        // adding each child node to HashMap key => value
                        contact.put("walk_id", walk_id);
                        contact.put("user_id", user_id);
                        contact.put("walk_date", walk_date);
                        contact.put("return_date", return_date);
                        contact.put("walk_time", walk_time);
                        contact.put("name", first_name + " " + last_name);


                        // adding contact to contact list
                        contactList.add(contact);
                    }
                } catch (final JSONException e) {

                }
            } else {

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            ListAdapter adapter = new SimpleAdapter(
                    getContext(), contactList,
                    R.layout.list_item3, new String[]{"walk_date","return_date", "walk_time", "name"}, new int[]{R.id.walk_date,R.id.return_date,
                    R.id.walk_time, R.id.name});

            lv.setAdapter(adapter);
        }

    }



}
