package doggy44.doggy44;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

/*        Intent i = new Intent(this, LoginActivity.class);
        startActivityForResult(i, 123);*/


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        /*fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Próximamente reservar paseo :)", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();

        if(MyApplicationG.rol == 1) { // CLIENTE
            nav_Menu.findItem(R.id.clientes).setVisible(false);
            nav_Menu.findItem(R.id.nuevopaseo).setVisible(false);
            nav_Menu.findItem(R.id.mispaseos).setVisible(false);

        }else { // PASEADOR
            nav_Menu.findItem(R.id.paseadores).setVisible(false);
            nav_Menu.findItem(R.id.paseos).setVisible(false);
            nav_Menu.findItem(R.id.misreservas).setVisible(false);
        }

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);

        // Llamar a valid token

        TextView navUseEmail = (TextView) headerView.findViewById(R.id.textView);
        navUseEmail.setText(MyApplicationG.email);

        TextView navUsername = (TextView) headerView.findViewById(R.id.nombre);
        navUsername.setText(MyApplicationG.first_name + " " + MyApplicationG.last_name);

        TextView navRol = (TextView) headerView.findViewById(R.id.rol);

        String rolNombre = "";
        if(MyApplicationG.rol == 1)
            rolNombre = "Cliente";
        else
            rolNombre = "Paseador";

        navRol.setText(rolNombre);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        int id = item.getItemId();

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();

        if (id == R.id.paseadores) {
            fragmentManager.beginTransaction().replace(R.id.main_0, new WalkersFragment()).commit();
        } else if (id == R.id.clientes) {
            fragmentManager.beginTransaction().replace(R.id.main_0, new CustomersFragment()).commit();
        } else if (id == R.id.paseos) {
            fragmentManager.beginTransaction().replace(R.id.main_0, new PaseosFragment()).commit();
        } else if (id == R.id.nuevopaseo) {
            fragmentManager.beginTransaction().replace(R.id.main_0, new AgregarPaseoFragment()).commit();
        } else if (id == R.id.mispaseos) {
            fragmentManager.beginTransaction().replace(R.id.main_0, new MisPaseosFragment()).commit();
        } else if (id == R.id.misreservas) {
            fragmentManager.beginTransaction().replace(R.id.main_0, new MisReservasFragment()).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
