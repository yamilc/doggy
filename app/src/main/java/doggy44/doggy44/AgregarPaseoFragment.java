package doggy44.doggy44;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;


public class AgregarPaseoFragment extends Fragment {
    public EditText walk_date;
    public EditText return_date;
    public EditText duracion;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_agregar_paseo, container, false);

        Button button = (Button) view.findViewById(R.id.btn_confirma);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                walk_date = (EditText) getActivity().findViewById(R.id.walk_date);
                return_date = (EditText) getActivity().findViewById(R.id.return_date);
                duracion = (EditText) getActivity().findViewById(R.id.duracion);


                //System.out.println("Logmagico " + walk_date.getText());

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);


                try {

                    String login_url = MyApplicationG.url + "newWalk";

                    URL url = new URL(login_url);
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    String post_data = URLEncoder.encode("walk_date","UTF-8")+"="+URLEncoder.encode(walk_date.getText().toString(),"UTF-8")+"&"
                            +URLEncoder.encode("return_date","UTF-8")+"="+URLEncoder.encode(return_date.getText().toString(),"UTF-8")+"&"
                            +URLEncoder.encode("walk_time","UTF-8")+"="+URLEncoder.encode(duracion.getText().toString(),"UTF-8")+"&"
                            +URLEncoder.encode("user_id","UTF-8")+"="+URLEncoder.encode(MyApplicationG.user_id,"UTF-8")+"&"
                            +URLEncoder.encode("active","UTF-8")+"="+URLEncoder.encode("1","UTF-8");


                    bufferedWriter.write(post_data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                    String result="";
                    String line="";
                    while((line = bufferedReader.readLine())!= null) {
                        result += line;
                    }

                    Toast.makeText(getActivity(),"HAS CREADO UN PASEO!",Toast.LENGTH_LONG).show();

                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });

        return view;
    }
}
